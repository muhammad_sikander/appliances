-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.2.5-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for shop
CREATE DATABASE IF NOT EXISTS `shop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `shop`;

-- Dumping structure for table shop.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'Slimline Dishwashers', '2018-04-30 15:56:14', '2018-04-30 15:56:17'),
	(2, 'Freestanding Dishwashers', '2018-04-30 15:57:47', '2018-04-30 15:57:49'),
	(3, 'Semi Integrated Dishwashers', '2018-04-30 15:57:53', '2018-04-30 15:57:55'),
	(4, 'Integrated Dishwashers ', '2018-04-30 15:58:06', '2018-04-30 15:58:08'),
	(5, 'Integrated Slimline Dishwashers', '2018-04-30 15:58:49', '2018-04-30 15:58:50');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table shop.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.migrations: ~8 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_04_30_200233_add_created_by_field_to_users_table', 2),
	(4, '2018_04_30_201340_create_product_table', 3),
	(5, '2018_04_30_225309_create_category_table', 4),
	(6, '2018_04_30_232655_add_foriegn_key_to_products_table', 5),
	(12, '2018_05_01_035358_add_sale_price_field_to_product_table', 6),
	(14, '2018_05_01_050434_create_wishlist_table', 7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table shop.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.password_resets: ~1 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('ranaadnan32112@gmail.com', '$2y$10$Au0NFcU12uQ6mR3iwWTm0uQn42oWmVRNlJz3Ok/LKHt/BO.qxIrZq', '2018-05-01 06:10:23');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table shop.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sale_price` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `products_cat_id_foreign` (`cat_id`),
  CONSTRAINT `products_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.products: ~6 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `title`, `image`, `price`, `brand`, `description`, `created_at`, `updated_at`, `cat_id`, `sale_price`) VALUES
	(1, 'Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'Montpellier.jpg', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 130),
	(2, 'Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'Montpellier.jpg', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 150),
	(3, 'Beko 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'beko.jpg', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 180),
	(4, 'Amica 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'amica.png', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 180),
	(5, 'Hotpoint 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'hotpoint.jpg', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 180),
	(6, 'Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P', 'montpellier_dishwasher.png', 200, 'beko.jpg', 'Simple and easy to use, this versatile dishwasher will make sure you never have to hand wash dishes again! The Montpellier 12 place setting dishwasher offers 5 different wash programmes for your convenience, meaning you\'ll have the flexibility to find the wash that\'s right for you.', '2018-04-30 16:40:30', '2018-04-30 16:40:32', 1, 180);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table shop.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `created_by`) VALUES
	(1, 'Muhammad Adnan', 'user@user.com', '$2y$10$GrYyLy4jBwEq4xqVVM4X..YBBhy6fwkkjfDI0iahn8qQv.HVQKHv6', 'DJ32iUSAfrXbeQRu8gr1s1PAkqr63RzF7WTNNeE7Tmy9SyaiU00Zbhw7Eoyu', '2018-04-30 21:09:35', '2018-04-30 21:09:35', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table shop.wishlists
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wishlists_product_id_foreign` (`product_id`),
  KEY `wishlists_user_id_foreign` (`user_id`),
  CONSTRAINT `wishlists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `wishlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shop.wishlists: ~0 rows (approximately)
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
