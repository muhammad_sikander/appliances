@extends('layouts.master')

@section('content')
    <div class="search-results col-md-9">
        <div class="search-results-header">
            <div class="row">
                <div class="col-xs-6"></div>
                <div class="col-xs-6 select-sort dropdown-control">
                    <select name="sort" id="search-sort" class="custom-select">
                        <option data-url="/dishwashers?page=1&amp;sort=price_asc" value="price_asc" selected="selected">Price low to high</option>
                        <option data-url="/dishwashers?page=1&amp;sort=price_desc" value="price_desc">Price high to low</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="search-results-list-separator"></div>
         @if(!empty($data['products']))
            @foreach($data['products'] as $product)
                <div class="search-results-product row">

                    <div class="product-image col-xs-4 col-sm-4">
                        <a href="#">
                        <img class="img-responsive" src="{{ asset('images/products')}}/{{$product->image}}" alt="Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P">
                        </a>
                    </div>
                    <div class="product-description col-xs-8 col-sm-8">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7 col-lg-8">
                            <a href="#">
                            <img class="article-brand" src="{{ asset('images/brands')}}/{{$product->brand}}" alt="Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P">
                            </a>
                            <h4><a href="#">{{$product->description}}</a></h4>
                            <div class="sales-container">
                            </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-lg-4">
                            <div class="col-xs-12">
                                <h5 class="price-previous">&pound;{{$product->sale_price}}</h5>
                                <h3 class="section-title">&pound;{{$product->price}}</h3>
                            </div>
                            <div class="col-xs-12">
                                <form action="#" method="post">
                                    <input type="hidden" name="_token" value="MsNpaItg65u70uMJeewYUiGoWCEYFuqXYE3edfN9">
                                    <button type="button" class="btn btn-block buy-now-btn"><i class="icons icon-cart-sm-white pull-right"></i>Buy Now </button>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif   
        <div id="pagination">
            {{ $data['products']->links() }}  
        </div>             
    </div>
@endsection
