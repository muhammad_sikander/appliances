    <div class="container">
        <div class="footer-top">
            <div class="row hidden-xs hidden-sm">
                <div class="col-md-3">
                    <img src="{{ asset('images/logo_small.png') }}" alt="AppliancesDelivered">
                    <ul class="fa-ul footer-info">
                        <li><span class="footer-info-text">Support: 01-5312270</span></li>
                        <li><span class="footer-info-text"><a href="https://www.appliancesdelivered.ie/help-advice/services/contact-us">Email Us! <i class="fa fa-commenting-o fa-flip-horizontal fa-fw"></i></a></span></li>
                        <li><span class="footer-info-text">Monday to Friday 8.30am - 5pm</span></li>
                        <li><span class="footer-info-text">Saturday 9am - 5pm (email only)</span></li>
                        <li><span class="footer-info-text">Sunday Closed</span></li>
                    </ul>
                </div>
                <div class="col-md-6 footer-site-map">
                    <div class="row">

                                                <div class="col-md-4">
                            <h4 class="section-title">Customer support</h4>
                            <ul class="list-unstyled">
                                <li><a href="https://www.appliancesdelivered.ie/help-advice/services/faq">FAQs</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/services/contact-us">Contact Us</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/services/returns-policy">Returns Policy</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/services/returns-cancellations">Returns &amp; Cancellations</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/flexi-fi-finance">Finance</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/delivery">Delivery Information</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/services/rearrange-delivery">Rearrange Delivery</a></li>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <h4 class="section-title">Info</h4>
                            <ul class="list-unstyled">
                                <li><a href="https://www.appliancesdelivered.ie/help-advice/info/installation">Installation</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/recycling">Recycling</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/about-us">About Us</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/price-match-promise">Price Match Promise</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/extended-warranty">Extended Warranty</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/info/careers">Careers</a></li>
<li><a href="https://www.appliancesdelivered.ie/commercial">Commercial</a></li>
                            </ul>
                        </div>

                        <div class="col-md-5">
                            <h4 class="section-title">Gift Cards</h4>
                            <ul class="list-unstyled">
                                <li><a href="https://www.appliancesdelivered.ie/help-advice/gift-card/purchase/design">Purchase Gift Card</a></li>
<li><a href="https://www.appliancesdelivered.ie/help-advice/gift-card/balance">Gift Card Balance</a></li>
                            </ul>

                            <h4 class="section-title">Persil Laundry Club</h4>
                            <ul class="list-unstyled">
                                <li><a href="https://www.appliancesdelivered.ie/laundry/persil-laundry-club/subscribe">Subscribe &amp; Save</a></li>
<li><a href="https://www.appliancesdelivered.ie/laundry/persil-laundry-club/manage">Manage Subscription</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <h4 class="section-title">Never Miss a Deal</h4>
                    <form id="subscribe-form" class="form-inline row" action="https://www.appliancesdelivered.ie/ajax/subscribe" method="POST">
                        <input type="hidden" name="_token" value="MsNpaItg65u70uMJeewYUiGoWCEYFuqXYE3edfN9">
                        <div class="form-group col-xs-7">
                            <label class="sr-only" for="subscribe">Subscribe</label>
                            <input type="email" class="form-control subscribe-email" name="email">
                        </div>
                        <div class="form-group col-xs-5">
                            <button type="submit" class="btn is-primary-green btn-block btn-subscribe">Subscribe</button>
                        </div>
                    </form>
                    <h4 class="section-title">Connect with us</h4>
                    <ul class="list-inline social-links">
                                                    <li><a href="https://www.facebook.com/AppliancesDelivered.ie/?fref=ts"><i class="icons icon-facebook"></i></a></li>
                                                    <li><a href="https://twitter.com/appliancesdeliv"><i class="icons icon-twitter"></i></a></li>
                                                    <li><a href="https://plus.google.com/b/103504325192767662545/103504325192767662545/about"><i class="icons icon-google-plus"></i></a></li>
                                                    <li><a href="https://www.pinterest.com/appliancesdeliv/"><i class="icons icon-pinterest"></i></a></li>
                                                    <li><a href="https://www.youtube.com/channel/UCqSQc2TvlCpGe8f4O9sWDlQ"><i class="icons icon-youtube"></i></a></li>
                                                    <li><a href="https://www.instagram.com/appliancesdelivered.ie/"><i class="icons icon-instagram"></i></a></li>
                                            </ul>
                </div>
            </div>

            <div class="footer-mobile-sections panel-group visible-xs visible-sm" role="tablist">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-appliancesdelivered-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-appliancesdelivered" aria-expanded="false" aria-controls="footer-appliancesdelivered">
                                AppliancesDelivered
                            </a>
                        </h4>
                    </div>
                    <div id="footer-appliancesdelivered" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-appliancesdelivered-header" aria-expanded="false">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="footer-info-text">Support: 01-5312270</span></li>
                            <li class="list-group-item"><span class="footer-info-text"><a href="https://www.appliancesdelivered.ie/services/contact-us">Email Us! <i class="fa fa-commenting-o fa-flip-horizontal fa-fw"></i></a></span></li>
                            <li class="list-group-item"><span class="footer-info-text">Monday to Friday 8.30am - 5pm</span></li>
                            <li class="list-group-item"><span class="footer-info-text">Saturday 9am - 5pm (email only)</span></li>
                            <li class="list-group-item"><span class="footer-info-text">Sunday Closed</span></li>
                        </ul>
                    </div>
                </div>

                                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-services-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-services" aria-expanded="false" aria-controls="footer-services">
                                Customer support
                            </a>
                        </h4>
                    </div>
                    <div id="footer-services" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-services-header" aria-expanded="false">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/services/faq">FAQs</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/services/contact-us">Contact Us</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/services/returns-policy">Returns Policy</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/services/returns-cancellations">Returns &amp; Cancellations</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/flexi-fi-finance">Finance</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/delivery">Delivery Information</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/services/rearrange-delivery">Rearrange Delivery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-info-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-info" aria-expanded="false" aria-controls="footer-info">
                                Info
                            </a>
                        </h4>
                    </div>
                    <div id="footer-info" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-info-header" aria-expanded="false">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/installation">Installation</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/recycling">Recycling</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/about-us">About Us</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/price-match-promise">Price Match Promise</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/extended-warranty">Extended Warranty</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/info/careers">Careers</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/commercial">Commercial</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-gift-card-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-gift-card" aria-expanded="false" aria-controls="footer-gift-card">
                                Gift Cards
                            </a>
                        </h4>
                    </div>
                    <div id="footer-gift-card" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-gift-card-header" aria-expanded="false">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/gift-card/purchase/design">Purchase Gift Card</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/help-advice/gift-card/balance">Gift Card Balance</a></li>
                        </ul>
                    </div>
                </div>

                                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-unilever-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-unilever" aria-expanded="false" aria-controls="footer-unilever">
                                Persil Laundry Club
                            </a>
                        </h4>
                    </div>
                    <div id="footer-unilever" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-unilever-header" aria-expanded="false">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="https://www.appliancesdelivered.ie/laundry/persil-laundry-club/subscribe">Subscribe &amp; Save</a></li>
<li class="list-group-item"><a href="https://www.appliancesdelivered.ie/laundry/persil-laundry-club/manage">Manage Subscription</a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-subscribe-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-subscribe" aria-expanded="false" aria-controls="footer-subscribe">
                                Never Miss a Deal
                            </a>
                        </h4>
                    </div>
                    <div id="footer-subscribe" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-subscribe-header" aria-expanded="false">
                        <form id="subscribe-form-mobile" class="form-inline row" action="https://www.appliancesdelivered.ie/ajax/subscribe" method="POST">
                            <input type="hidden" name="_token" value="MsNpaItg65u70uMJeewYUiGoWCEYFuqXYE3edfN9">
                            <div class="form-group col-sm-8">
                                <label class="sr-only" for="subscribe">Email Subscribe</label>
                                <input type="email" class="form-control subscribe-email" name="email">
                            </div>
                            <div class="form-group col-sm-4">
                                <button type="submit" class="btn is-primary-green btn-block btn-subscribe">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="footer-connect-header">
                        <h4 class="panel-title">
                            <a class="collapsed section-toggle" role="button" data-toggle="collapse" href="#footer-connect" aria-expanded="false" aria-controls="footer-connect">
                                Connect with us
                            </a>
                        </h4>
                    </div>
                    <div id="footer-connect" class="panel-collapse collapse" role="tabpanel" aria-labelledby="footer-connect-header" aria-expanded="false">
                        <ul class="list-inline center-table">
                                                            <li><a href="https://www.facebook.com/AppliancesDelivered.ie/?fref=ts"><i class="icons icon-facebook"></i></a></li>
                                                            <li><a href="https://twitter.com/appliancesdeliv"><i class="icons icon-twitter"></i></a></li>
                                                            <li><a href="https://plus.google.com/b/103504325192767662545/103504325192767662545/about"><i class="icons icon-google-plus"></i></a></li>
                                                            <li><a href="https://www.pinterest.com/appliancesdeliv/"><i class="icons icon-pinterest"></i></a></li>
                                                            <li><a href="https://www.youtube.com/channel/UCqSQc2TvlCpGe8f4O9sWDlQ"><i class="icons icon-youtube"></i></a></li>
                                                            <li><a href="https://www.instagram.com/appliancesdelivered.ie/"><i class="icons icon-instagram"></i></a></li>
                                                    </ul>
                    </div>
                </div>
            </div>
            <div class="row row-relative">
                <div class="col-md-6 col-md-offset-3 col-align-bottom">
                    <div class="center-table hidden-xs hidden-sm payment-icons">
                        <img class="payment-options" src="https://www.appliancesdelivered.ie/images/payment-options.jpg" alt="We accept multiple payment options">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-9 col-sm-center">
                    <div class="trust-container">
                        <a href="http://www.ecommercetrustmark.eu/" target="_blank">
                            <img class="" src="https://www.appliancesdelivered.ie/images/trust-ecommerce-europe.jpg" alt="Trust E-commerce Europe">
                        </a>
                        <a href="http://www.retailexcellence.ie/ecommerce/about-the-trustmark/about-the-trustmark/" target="_blank">
                            <img class="" src="https://www.appliancesdelivered.ie/images/trust-mark-2018.jpg" alt="Retail Excellence 2018 Trust Mark">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
    <ul class="list-inline center-table footer-bottom-links">
        <li><a href="https://www.appliancesdelivered.ie/services/cookie-policy">Cookie Policy</a></li>
        <li><a href="https://www.appliancesdelivered.ie/services/terms-and-conditions">Terms &amp; Conditions</a></li>
    </ul>

    
    <div class="copyright">© 2018 AppliancesDelivered.ie.</div>

    <div id="backtothetop" class="row hidden-md hidden-lg pull-right"><a></a></div>
</div>    </div>
