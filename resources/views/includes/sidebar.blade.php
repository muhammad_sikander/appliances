<div class="col-md-3 hidden-sm hidden-xs">
   <div class="search-filters">
      <div class="search-filters-title">
         <a href="/search" class="btn btn-link btn-xs btn-block filter-link">Clear All</a>
      </div>
      <div class="search-filters-container">
         <div class="filter-control">
            <p class="control-title">Categories</p>
            <p class="control-clear">
               <a href="/search?sort=price_asc">[clear]</a>
            </p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-categories">
                @if(!empty($data['category']))
                    @foreach($data['category'] as $category)
                        <div class="control-list-item">
                            <input id="{{$category->id}}" name="refine-filter-refine-filter-categories" type="radio" class="link-input" data-url="/dishwashers/slimline">
                            <label for="{{$category->id}}"><span></span> {{$category->title}} (5)</label>
                        </div>
                    @endforeach
                @endif    
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Price</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-prices">
               <div class="control-list-item">
                  <input id="p0" name="refine-filter-refine-filter-prices" type="checkbox" class="link-input" data-url="/search/dishwashers/price_200_to_400?sort=price_asc">
                  <label for="p0"><span></span> €200 - €400 (27)</label>
               </div>
               <div class="control-list-item">
                  <input id="p1" name="refine-filter-refine-filter-prices" type="checkbox" class="link-input" data-url="/search/dishwashers/price_400_to_600?sort=price_asc">
                  <label for="p1"><span></span> €400 - €600 (6)</label>
               </div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Brand</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-brand">
               <div class="control-list-item">
                  <input id="refine-filter-brand-1" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_montpellier?sort=price_asc">
                  <label for="refine-filter-brand-1"><span></span> Montpellier (12)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-brand-2" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_whirlpool?sort=price_asc">
                  <label for="refine-filter-brand-2"><span></span> Whirlpool (5)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-brand-3" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_hotpoint?sort=price_asc">
                  <label for="refine-filter-brand-3"><span></span> Hotpoint (3)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-brand-4" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_amica?sort=price_asc">
                  <label for="refine-filter-brand-4"><span></span> Amica (2)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-brand-5" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_beko?sort=price_asc">
                  <label for="refine-filter-brand-5"><span></span> Beko (2)</label>
               </div>
               <div class="collapse" id="desktop-search-filter-refine-filter-brand-more">
                  <div class="control-list-item">
                     <input id="refine-filter-brand-6" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_bosch?sort=price_asc">
                     <label for="refine-filter-brand-6"><span></span> Bosch (2)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-brand-7" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_gorenje?sort=price_asc">
                     <label for="refine-filter-brand-7"><span></span> Gorenje (2)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-brand-8" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_indesit?sort=price_asc">
                     <label for="refine-filter-brand-8"><span></span> Indesit (2)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-brand-9" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_culina?sort=price_asc">
                     <label for="refine-filter-brand-9"><span></span> Culina (1)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-brand-10" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_samsung?sort=price_asc">
                     <label for="refine-filter-brand-10"><span></span> Samsung (1)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-brand-11" name="refine-filter-refine-filter-brand" type="checkbox" class="link-input" data-url="/search/dishwashers/brand_zanussi?sort=price_asc">
                     <label for="refine-filter-brand-11"><span></span> Zanussi (1)</label>
                  </div>
               </div>
               <div class="control-list-expand collapsed" data-toggle="collapse" data-target="#desktop-search-filter-refine-filter-brand-more"></div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Fit Type</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-fit-type">
               <div class="control-list-item">
                  <input id="refine-filter-fit-type-1" name="refine-filter-refine-filter-fit-type" type="checkbox" class="link-input" data-url="/search/dishwashers/fit-type_freestanding?sort=price_asc">
                  <label for="refine-filter-fit-type-1"><span></span> Freestanding (16)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-fit-type-2" name="refine-filter-refine-filter-fit-type" type="checkbox" class="link-input" data-url="/search/dishwashers/fit-type_integrated?sort=price_asc">
                  <label for="refine-filter-fit-type-2"><span></span> Integrated (15)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-fit-type-3" name="refine-filter-refine-filter-fit-type" type="checkbox" class="link-input" data-url="/search/dishwashers/fit-type_semi+integrated?sort=price_asc">
                  <label for="refine-filter-fit-type-3"><span></span> Semi Integrated (2)</label>
               </div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Place Settings</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-place-settings">
               <div class="control-list-item">
                  <input id="refine-filter-place-settings-1" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_12?sort=price_asc">
                  <label for="refine-filter-place-settings-1"><span></span> 12 (12)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-place-settings-2" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_10?sort=price_asc">
                  <label for="refine-filter-place-settings-2"><span></span> 10 (10)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-place-settings-3" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_13?sort=price_asc">
                  <label for="refine-filter-place-settings-3"><span></span> 13 (4)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-place-settings-4" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_14?sort=price_asc">
                  <label for="refine-filter-place-settings-4"><span></span> 14 (3)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-place-settings-5" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_9?sort=price_asc">
                  <label for="refine-filter-place-settings-5"><span></span> 9 (3)</label>
               </div>
               <div class="collapse" id="desktop-search-filter-refine-filter-place-settings-more">
                  <div class="control-list-item">
                     <input id="refine-filter-place-settings-6" name="refine-filter-refine-filter-place-settings" type="checkbox" class="link-input" data-url="/search/dishwashers/place-settings_15?sort=price_asc">
                     <label for="refine-filter-place-settings-6"><span></span> 15 (1)</label>
                  </div>
               </div>
               <div class="control-list-expand collapsed" data-toggle="collapse" data-target="#desktop-search-filter-refine-filter-place-settings-more"></div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Quick Wash</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-quick-wash">
               <div class="control-list-item">
                  <input id="refine-filter-quick-wash-1" name="refine-filter-refine-filter-quick-wash" type="checkbox" class="link-input" data-url="/search/dishwashers/quick-wash_yes?sort=price_asc">
                  <label for="refine-filter-quick-wash-1"><span></span> Yes (25)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-quick-wash-2" name="refine-filter-refine-filter-quick-wash" type="checkbox" class="link-input" data-url="/search/dishwashers/quick-wash_no?sort=price_asc">
                  <label for="refine-filter-quick-wash-2"><span></span> No (5)</label>
               </div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Capacity</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-capacity">
               <div class="control-list-item">
                  <input id="refine-filter-capacity-1" name="refine-filter-refine-filter-capacity" type="checkbox" class="link-input" data-url="/search/dishwashers/capacity_12l?sort=price_asc">
                  <label for="refine-filter-capacity-1"><span></span> 12l (5)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-capacity-2" name="refine-filter-refine-filter-capacity" type="checkbox" class="link-input" data-url="/search/dishwashers/capacity_11l?sort=price_asc">
                  <label for="refine-filter-capacity-2"><span></span> 11l (3)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-capacity-3" name="refine-filter-refine-filter-capacity" type="checkbox" class="link-input" data-url="/search/dishwashers/capacity_10l?sort=price_asc">
                  <label for="refine-filter-capacity-3"><span></span> 10l (2)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-capacity-4" name="refine-filter-refine-filter-capacity" type="checkbox" class="link-input" data-url="/search/dishwashers/capacity_10.5l?sort=price_asc">
                  <label for="refine-filter-capacity-4"><span></span> 10.5l (1)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-capacity-5" name="refine-filter-refine-filter-capacity" type="checkbox" class="link-input" data-url="/search/dishwashers/capacity_6.5l?sort=price_asc">
                  <label for="refine-filter-capacity-5"><span></span> 6.5l (1)</label>
               </div>
            </div>
         </div>
         <div class="filter-control">
            <p class="control-title">Half Load</p>
            <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-half-load">
               <div class="control-list-item">
                  <input id="refine-filter-half-load-1" name="refine-filter-refine-filter-half-load" type="checkbox" class="link-input" data-url="/search/dishwashers/half-load_yes?sort=price_asc">
                  <label for="refine-filter-half-load-1"><span></span> Yes (14)</label>
               </div>
               <div class="control-list-item">
                  <input id="refine-filter-half-load-2" name="refine-filter-refine-filter-half-load" type="checkbox" class="link-input" data-url="/search/dishwashers/half-load_no?sort=price_asc">
                  <label for="refine-filter-half-load-2"><span></span> No (10)</label>
               </div>
            </div>
         </div>
         <div class="more-filters-container collapse" id="desktop-more">
            <div class="filter-control">
               <p class="control-title">Warranty</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-warranty">
                  <div class="control-list-item">
                     <input id="refine-filter-warranty-1" name="refine-filter-refine-filter-warranty" type="checkbox" class="link-input" data-url="/search/dishwashers/warranty_2+years?sort=price_asc">
                     <label for="refine-filter-warranty-1"><span></span> 2 Years (24)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-warranty-2" name="refine-filter-refine-filter-warranty" type="checkbox" class="link-input" data-url="/search/dishwashers/warranty_1+year?sort=price_asc">
                     <label for="refine-filter-warranty-2"><span></span> 1 Year (8)</label>
                  </div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Width</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-width">
                  <div class="control-list-item">
                     <input id="refine-filter-width-1" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_600mm?sort=price_asc">
                     <label for="refine-filter-width-1"><span></span> 600mm (10)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-width-2" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_450mm?sort=price_asc">
                     <label for="refine-filter-width-2"><span></span> 450mm (8)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-width-3" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_598mm?sort=price_asc">
                     <label for="refine-filter-width-3"><span></span> 598mm (6)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-width-4" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_595mm?sort=price_asc">
                     <label for="refine-filter-width-4"><span></span> 595mm (3)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-width-5" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_445mm?sort=price_asc">
                     <label for="refine-filter-width-5"><span></span> 445mm (2)</label>
                  </div>
                  <div class="collapse" id="desktop-search-filter-refine-filter-width-more">
                     <div class="control-list-item">
                        <input id="refine-filter-width-6" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_448mm?sort=price_asc">
                        <label for="refine-filter-width-6"><span></span> 448mm (2)</label>
                     </div>
                     <div class="control-list-item">
                        <input id="refine-filter-width-7" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_446mm?sort=price_asc">
                        <label for="refine-filter-width-7"><span></span> 446mm (1)</label>
                     </div>
                     <div class="control-list-item">
                        <input id="refine-filter-width-8" name="refine-filter-refine-filter-width" type="checkbox" class="link-input" data-url="/search/dishwashers/width_596mm?sort=price_asc">
                        <label for="refine-filter-width-8"><span></span> 596mm (1)</label>
                     </div>
                  </div>
                  <div class="control-list-expand collapsed" data-toggle="collapse" data-target="#desktop-search-filter-refine-filter-width-more"></div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Customer Rating</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-customer-rating">
                  <div class="control-list-item">
                     <input id="g5" name="refine-filter-refine-filter-customer-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/rating_5?sort=price_asc">
                     <label for="g5"><span></span> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> (3)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="g4" name="refine-filter-refine-filter-customer-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/rating_4?sort=price_asc">
                     <label for="g4"><span></span> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> (1)</label>
                  </div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Colour</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-colour">
                  <div class="control-list-item">
                     <input id="refine-filter-colour-1" name="refine-filter-refine-filter-colour" type="checkbox" class="link-input" data-url="/search/dishwashers/colour_white?sort=price_asc">
                     <label for="refine-filter-colour-1">
                        <span></span>
                        <div class="filter-colour-box filter-colour-box_white"></div>
                        White (16)
                     </label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-colour-2" name="refine-filter-refine-filter-colour" type="checkbox" class="link-input" data-url="/search/dishwashers/colour_black?sort=price_asc">
                     <label for="refine-filter-colour-2">
                        <span></span>
                        <div class="filter-colour-box filter-colour-box_black"></div>
                        Black (3)
                     </label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-colour-3" name="refine-filter-refine-filter-colour" type="checkbox" class="link-input" data-url="/search/dishwashers/colour_silver?sort=price_asc">
                     <label for="refine-filter-colour-3">
                        <span></span>
                        <div class="filter-colour-box filter-colour-box_silver"></div>
                        Silver (2)
                     </label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-colour-4" name="refine-filter-refine-filter-colour" type="checkbox" class="link-input" data-url="/search/dishwashers/colour_stainless+steel?sort=price_asc">
                     <label for="refine-filter-colour-4">
                        <span></span>
                        <div class="filter-colour-box filter-colour-box_stainless-steel"></div>
                        Stainless Steel (2)
                     </label>
                  </div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Energy Rating</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-energy-rating">
                  <div class="control-list-item">
                     <input id="refine-filter-energy-rating-1" name="refine-filter-refine-filter-energy-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/energy-rating_a%2B?sort=price_asc">
                     <label for="refine-filter-energy-rating-1"><span></span> A+ (18)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-energy-rating-2" name="refine-filter-refine-filter-energy-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/energy-rating_a%2B%2B?sort=price_asc">
                     <label for="refine-filter-energy-rating-2"><span></span> A++ (12)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-energy-rating-3" name="refine-filter-refine-filter-energy-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/energy-rating_a?sort=price_asc">
                     <label for="refine-filter-energy-rating-3"><span></span> A (2)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-energy-rating-4" name="refine-filter-refine-filter-energy-rating" type="checkbox" class="link-input" data-url="/search/dishwashers/energy-rating_a%2B%2B%2B?sort=price_asc">
                     <label for="refine-filter-energy-rating-4"><span></span> A+++ (1)</label>
                  </div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Height</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-height">
                  <div class="control-list-item">
                     <input id="refine-filter-height-1" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_850mm?sort=price_asc">
                     <label for="refine-filter-height-1"><span></span> 850mm (13)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-height-2" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_820mm?sort=price_asc">
                     <label for="refine-filter-height-2"><span></span> 820mm (12)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-height-3" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_815mm?sort=price_asc">
                     <label for="refine-filter-height-3"><span></span> 815mm (3)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-height-4" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_845mm?sort=price_asc">
                     <label for="refine-filter-height-4"><span></span> 845mm (2)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-height-5" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_805mm?sort=price_asc">
                     <label for="refine-filter-height-5"><span></span> 805mm (1)</label>
                  </div>
                  <div class="collapse" id="desktop-search-filter-refine-filter-height-more">
                     <div class="control-list-item">
                        <input id="refine-filter-height-6" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_818mm?sort=price_asc">
                        <label for="refine-filter-height-6"><span></span> 818mm (1)</label>
                     </div>
                     <div class="control-list-item">
                        <input id="refine-filter-height-7" name="refine-filter-refine-filter-height" type="checkbox" class="link-input" data-url="/search/dishwashers/height_840mm?sort=price_asc">
                        <label for="refine-filter-height-7"><span></span> 840mm (1)</label>
                     </div>
                  </div>
                  <div class="control-list-expand collapsed" data-toggle="collapse" data-target="#desktop-search-filter-refine-filter-height-more"></div>
               </div>
            </div>
            <div class="filter-control">
               <p class="control-title">Depth</p>
               <div class="controls-list collapse in" id="desktop-search-filter-refine-filter-depth">
                  <div class="control-list-item">
                     <input id="refine-filter-depth-1" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_600mm?sort=price_asc">
                     <label for="refine-filter-depth-1"><span></span> 600mm (14)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-depth-2" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_550mm?sort=price_asc">
                     <label for="refine-filter-depth-2"><span></span> 550mm (7)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-depth-3" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_570mm?sort=price_asc">
                     <label for="refine-filter-depth-3"><span></span> 570mm (4)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-depth-4" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_573mm?sort=price_asc">
                     <label for="refine-filter-depth-4"><span></span> 573mm (3)</label>
                  </div>
                  <div class="control-list-item">
                     <input id="refine-filter-depth-5" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_555mm?sort=price_asc">
                     <label for="refine-filter-depth-5"><span></span> 555mm (2)</label>
                  </div>
                  <div class="collapse" id="desktop-search-filter-refine-filter-depth-more">
                     <div class="control-list-item">
                        <input id="refine-filter-depth-6" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_540mm?sort=price_asc">
                        <label for="refine-filter-depth-6"><span></span> 540mm (1)</label>
                     </div>
                     <div class="control-list-item">
                        <input id="refine-filter-depth-7" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_558mm?sort=price_asc">
                        <label for="refine-filter-depth-7"><span></span> 558mm (1)</label>
                     </div>
                     <div class="control-list-item">
                        <input id="refine-filter-depth-8" name="refine-filter-refine-filter-depth" type="checkbox" class="link-input" data-url="/search/dishwashers/depth_596mm?sort=price_asc">
                        <label for="refine-filter-depth-8"><span></span> 596mm (1)</label>
                     </div>
                  </div>
                  <div class="control-list-expand collapsed" data-toggle="collapse" data-target="#desktop-search-filter-refine-filter-depth-more"></div>
               </div>
            </div>
         </div>
         <div class="more-filters-button">
            <a id="desktopmore-button" class="control-toggle collapsed" data-toggle="collapse" data-target="#desktop-more">More Filters</a>
         </div>
      </div>
   </div>
</div>