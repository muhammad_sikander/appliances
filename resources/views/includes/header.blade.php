<div class="header">
        <div class="media overflow-visible">
            <div id="main-menu-mobile-btn" class="media-left media-middle visible-xs visible-sm">
                <button id="mobile-menu-btn" type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="media-left media-middle">
                <a href="{{ route('home') }}">
                    <img class="hidden-xs hidden-sm" src="{{ asset('images/logo.png') }}" alt="AppliancesDelivered">
                    <img id="logo-ad-mobile-version" class="visible-xs visible-sm" src="{{ asset('images/logo_small.png') }}" alt="AppliancesDelivered">
                </a>
            </div>
            <div class="media-body media-middle overflow-visible">
                <div class="header-bottom row hidden-xs">
                    <div class="col-sm-9 col-md-8 col-lg-9">
                        <div class="input-inner input-inner-right header-search header-blue">
    <form action="https://www.appliancesdelivered.ie/search">
        <span class="input-inner-addon"><i class="fa fa-search fa-lg"></i></span>
        <input class="form-control" type="search" name="find" placeholder="Search for products" autocomplete="off">
    </form>
</div>

                                                    <!-- TrustBox widget - Drop-Down -->
<div class="trustpilot-widget" data-locale="en-US" data-template-id="5418052cfbfb950d88702476" data-businessunit-id="57dd763f0000ff000594d470" data-style-height="30px" data-style-width="300px" data-theme="light" data-stars="4,5" style="position: relative;"><iframe frameborder="0" scrolling="no" title="Customer reviews powered by Trustpilot" src="https://widget.trustpilot.com/trustboxes/5418052cfbfb950d88702476/index.html?locale=en-US&amp;templateId=5418052cfbfb950d88702476&amp;businessunitId=57dd763f0000ff000594d470&amp;styleHeight=30px&amp;styleWidth=300px&amp;theme=light&amp;stars=4%2C5" style="position: relative; height: 30px; width: 300px; border-style: none; display: block; overflow: hidden;"></iframe><iframe frameborder="0" scrolling="no" title="Customer reviews powered by Trustpilot" src="https://widget.trustpilot.com/trustboxes/5418052cfbfb950d88702476/popup.html?locale=en-US&amp;templateId=5418052cfbfb950d88702476&amp;businessunitId=57dd763f0000ff000594d470&amp;styleHeight=30px&amp;styleWidth=300px&amp;theme=light&amp;stars=4%2C5" style="position: absolute; z-index: 99; top: 25px; height: 400px; width: 300px; border-style: none; display: none; overflow: hidden;"></iframe></div>
<!-- End TrustBox widget -->                                            </div>
                    <div class="col-sm-3 col-md-4 col-lg-3">
                        <div class="pull-right">
                            <a href="https://www.appliancesdelivered.ie/basket" class="center-table">
                                <div class="feature">
                                    <div class="feature-icon">
                                        <i class="icons icon-cart"></i>
                                    </div>
                                    <div class="feature-text cart-text">
                                        <p>basket (0)</p>
                                        <p class="hidden-sm">€0.00</p>
                                   </div>
                               </div>
                           </a>
                        </div>
                        <div class="pull-right">
                            <div class="feature">
                                <div class="feature-icon">
                                    <i class="icons icon-phone-blue"></i>
                                </div>
                                <div class="feature-text cart-text">
                                    <p><a href="{{ route('logout') }}"> Logout</a></p>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="header-mobile list-inline visible-xs-inline-block">
                    <li><a href="tel:+35315312270"><i class="icons icon-phone-grey"></i></a></li>
                    <li><a href="https://www.appliancesdelivered.ie/basket"><i class="icons icon-cart-grey"></i><span class="badge">0</span></a></li>
                </ul>
            </div>
        </div>
        <div class="visible-xs">
            <div class="input-inner input-inner-right header-search header-blue">
    <form action="https://www.appliancesdelivered.ie/search">
        <span class="input-inner-addon"><i class="fa fa-search fa-lg"></i></span>
        <input class="form-control" type="search" name="find" placeholder="Search for products" autocomplete="off">
    </form>
</div>
        </div>
    </div>
<nav class="navbar navbar-default menu-directory-navbar hidden-xs hidden-sm">
	<ul class="nav nav-justified">
        <li>
            <a href="{{route('home')}}" class="menu-directory-item" data-target="0">Home</a>
        </li>
                <li>
            <a href="/dishwashers" class="menu-directory-item" data-target="1">Dishwashers</a>
        </li>
                <li>
            <a href="/fridges-and-freezers" class="menu-directory-item" data-target="2">Refrigerators</a>
        </li>
                <li>
            <a href="/cooking" class="menu-directory-item" data-target="3">Cooking</a>
        </li>
                <li>
            <a href="/floorcare" class="menu-directory-item" data-target="4">Floorcare</a>
        </li>
                <li>
            <a href="/small-appliances" class="menu-directory-item" data-target="5">Small Appliances</a>
        </li>
                <li>
            <a href="/garden-diy" class="menu-directory-item" data-target="6">Garden &amp; DIY</a>
        </li>
                <li>
            <a href="javascript:void(0);" class="menu-directory-item" data-target="7">Help &amp; Advice</a>
        </li>
    </ul>
</nav>